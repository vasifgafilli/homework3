package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String[][] week = new String[7][2];

        week[0][0] = "monday";
        week[1][0] = "tuesday";
        week[2][0] = "wednesday";
        week[3][0] = "thursday";
        week[4][0] = "friday";
        week[5][0] = "saturday";
        week[6][0] = "sunday";

        week[0][1] = "go to football";
        week[1][1] = "go to work";
        week[2][1] = "doing homework";
        week[3][1] = "go to course";
        week[4][1] = "work hard";
        week[5][1] = "resting";
        week[6][1] = "master resting";


        System.out.println("Please, input the day of the week:");
        Scanner day = new Scanner(System.in);
        while (true) {

            switch (day.nextLine())
            {
                case "monday":
                    System.out.println("Your tasks for " + week[0][0] + ": " + week[0][1] + ".");
                    break;
                case "tuesday":
                    System.out.println("Your tasks for " + week[1][0] + ": " + week[1][1] + ".");
                    break;
                case "wednesday":
                    System.out.println("Your tasks for " + week[2][0] + ": " + week[2][1] + ".");
                    break;
                case "thursday":
                    System.out.println("Your tasks for " + week[3][0] + ": " + week[3][1] + ".");
                    break;
                case "friday":
                    System.out.println("Your tasks for " + week[4][0] + ": " + week[4][1] + ".");
                    break;
                case "saturday":
                    System.out.println("Your tasks for " + week[5][0] + ": " + week[5][1] + ".");
                    break;
                case "sunday":
                    System.out.println("Your tasks for " + week[6][0] + ": " + week[6][1] + ".");
                    break;
                case "exit":
                    return;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    System.out.println("Please, input the day of the week:");
            }
        }
    }
}
